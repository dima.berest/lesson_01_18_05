/*
1. ������� ����� Stack, ������� ����� ������������� ������� ������ ��������� ������ ����. 
������ ����� �� ������ ���� ��������� (����������� ������������ ������).

2. ����������� ��� ������ ���� ������ �� ����� (int, float, double, string). 
������ ���� ��� ������� ������ 
pop(), ����� ������� �� ����� ������� �������, 
� push(), ����� �������� ����� �������.
*/

#include <iostream>

class Stack
{
public:

    ~Stack()
    {
        delete[] stack;
    }

    void push(int value)
    {
        if (size == capacity)   // if out of mem
            allocateMemory();

        stack[size] = value;
        ++size;
    }

    void pop()
    {
        if (size == 0)          // if out of elements
            std::cout << "Stack is empty." << "\n\n";
        else
        {
            --size;
            std::cout << "You got " << stack[size] << '.' << "\n\n";
        }
    }

private:

    int* stack = nullptr;  // stack ptr
    int capacity = 0;      // stack capacity
    int size = 0;          // num of elements

    void allocateMemory()
    {
        if (capacity == 0)      // if no mem ever was
            capacity = 2;   
        else
            capacity *= 2;

        int* stack_new = new int[capacity];     // new mem allocated
        for (int i = 0; i < size; ++i)          
            stack_new[i] = stack[i];            // copy old to new
        delete[] stack;                         // del old
        stack = stack_new;                      // rename new
    }
};

void checkInput()
{
    std::cin.ignore(32767, '\n');	    // del previous input buffer
    if (std::cin.fail())	            // if cin failed
    {
        std::cin.clear();	            // "restart" cin
        std::cin.ignore(32767, '\n');	// del previous input buffer
    }
}

int main()
{
    Stack tmp;
    int pushValue = 0;
    int action = 0;

    // input for stack test:
    while (1)
    {
        std::cout << "Push(1) or Pop(2)? Enter 1 or 2: ";
        std::cin >> action;
        checkInput();
        if (action == 1)
        {
            while (1)
            {
                std::cout << "Enter an int > 0 and < 100 to Push: ";
                std::cin >> pushValue;
                checkInput();
                if (pushValue < 0 || pushValue > 100)
                    std::cout << "Incorrect!\n";
                else
                {
                    tmp.push(pushValue);
                    std::cout << "\n";
                    break;
                }
            }
        }
        else if (action == 2)
        {
            tmp.pop();
        }
        else std::cout << "Incorrect!\n";
    }

    return 0;
}